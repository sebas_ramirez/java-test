
import com.test.java.test.service.CompraProceso;
import com.test.java.test.service.ProductoEstrellaProceso;
import com.test.java.test.service.VentaProceso;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Scanner;

public class JavaTest {

    private static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {

        int opc;

        do {
            System.out.println("**MENU**\n");
            System.out.println("1.VENDER");
            System.out.println("2.COMPRAR");
            System.out.println("3.PRODUCTO ESTRELLA");
            System.out.println("4.SALIR");
            System.out.println("\nINGRESAR OPCION:");
            opc = sc.nextInt();

            if (opc == 1) {
                Vender();

            } else if (opc == 2) {
                Comprar();

            } else if (opc == 3) {
                ProductoEstrella();

            }
        } while (opc != 4);
    }

    public static void Vender() throws SQLException {

        VentaProceso ventaProceso = new VentaProceso();

        System.out.println("Ingresar id de producto:\n");
        Integer productoId = sc.nextInt();

        System.out.println("Ingresar cantidad:\n");
        Integer productoCantidad = sc.nextInt();

        ventaProceso.procesoVenta(productoId, productoCantidad, Date.valueOf(LocalDate.now()));
    }

    public static void Comprar() throws SQLException {

        CompraProceso compraProceso = new CompraProceso();

        System.out.println("Ingresar id de producto:\n");
        Integer productoId = sc.nextInt();

        System.out.println("Ingresar cantidad:\n");
        Integer productoCantidad = sc.nextInt();

        compraProceso.procesoCompra(productoId, productoCantidad, Date.valueOf(LocalDate.now()));

    }

    public static void ProductoEstrella() throws SQLException {
        
        ProductoEstrellaProceso productoEstrellaProceso = new ProductoEstrellaProceso();
        System.out.println("El producto estrella es: ");
        productoEstrellaProceso.procesoProductoEstrella();
        
    }
}
