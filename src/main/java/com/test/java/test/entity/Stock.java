package com.test.java.test.entity;

public class Stock {

    private Integer stockId;
    private Integer stockCantidad;
    private Integer productoId;

    public Stock() {
    }

    public Stock(Integer stockId) {
        this.stockId = stockId;
    }

    public Stock(Integer stockId, Integer stockCantidad) {
        this.stockId = stockId;
        this.stockCantidad = stockCantidad;
    }

    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    public Integer getStockCantidad() {
        return stockCantidad;
    }

    public void setStockCantidad(Integer stockCantidad) {
        this.stockCantidad = stockCantidad;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    @Override
    public String toString() {
        return "";
    }
}
