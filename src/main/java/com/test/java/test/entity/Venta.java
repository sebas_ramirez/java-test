package com.test.java.test.entity;

import java.sql.Date;

public class Venta {

    private Integer ventaId;
    private Integer ventaCantidad;
    private Date ventaFecha;
    private Integer productoId;

    private String productoDescripcion;

    public String getProductoDescripcion() {
        return productoDescripcion;
    }

    public void setProductoDescripcion(String productoDescripcion) {
        this.productoDescripcion = productoDescripcion;
    }

    public Venta() {
    }

    public Venta(Integer ventaId) {
        this.ventaId = ventaId;
    }

    public Venta(Integer ventaId, Integer ventaCantidad, Date ventaFecha) {
        this.ventaId = ventaId;
        this.ventaCantidad = ventaCantidad;
        this.ventaFecha = ventaFecha;
    }

    public Integer getVentaId() {
        return ventaId;
    }

    public void setVentaId(Integer ventaId) {
        this.ventaId = ventaId;
    }

    public Integer getVentaCantidad() {
        return ventaCantidad;
    }

    public void setVentaCantidad(Integer ventaCantidad) {
        this.ventaCantidad = ventaCantidad;
    }

    public Date getVentaFecha() {
        return ventaFecha;
    }

    public void setVentaFecha(Date ventaFecha) {
        this.ventaFecha = ventaFecha;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    @Override
    public String toString() {
        return "";
    }
}
