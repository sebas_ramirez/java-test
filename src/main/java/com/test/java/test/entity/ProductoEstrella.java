package com.test.java.test.entity;

import java.sql.Date;
import java.time.LocalDate;


public class ProductoEstrella {

    private String productoEstrellaNombre;
    private Integer productoEstrellaCantidadVendido;
    private Date productoEstrellaFechaReporte;

    public ProductoEstrella(String productoEstrellaNombre, Integer productoEstrellaCantidadVendido, Date productoEstrellaFechaReporte) {
        this.productoEstrellaNombre = productoEstrellaNombre;
        this.productoEstrellaCantidadVendido = productoEstrellaCantidadVendido;
        this.productoEstrellaFechaReporte = productoEstrellaFechaReporte;
    }

    public ProductoEstrella() {
        this.productoEstrellaFechaReporte = Date.valueOf(LocalDate.now());
        }

    public String getProductoEstrellaNombre() {
        return productoEstrellaNombre;
    }

    public void setProductoEstrellaNombre(String productoEstrellaNombre) {
        this.productoEstrellaNombre = productoEstrellaNombre;
    }

    public Integer getProductoEstrellaCantidadVendido() {
        return productoEstrellaCantidadVendido;
    }

    public void setProductoEstrellaCantidadVendido(Integer productoEstrellaCantidadVendido) {
        this.productoEstrellaCantidadVendido = productoEstrellaCantidadVendido;
    }

    public Date getProductoEstrellaFechaReporte() {
        return productoEstrellaFechaReporte;
    }

    public void setProductoEstrellaFechaReporte(Date productoEstrellaFechaReporte) {
        this.productoEstrellaFechaReporte = productoEstrellaFechaReporte;
    }

    @Override
    public String toString() {
        return "";
    }
}
