package com.test.java.test.entity;

import java.sql.Date;

public class Compra {

    private Integer compraId;
    private Integer compraCantidad;
    private Date compraFecha;
    private Integer productoId;

    public Compra() {
    }

    public Compra(Integer compraId) {
        this.compraId = compraId;
    }

    public Compra(Integer compraId, Integer compraCantidad, Date compraFecha) {
        this.compraId = compraId;
        this.compraCantidad = compraCantidad;
        this.compraFecha = compraFecha;
    }

    public Integer getCompraId() {
        return compraId;
    }

    public void setCompraId(Integer compraId) {
        this.compraId = compraId;
    }

    public Integer getCompraCantidad() {
        return compraCantidad;
    }

    public void setCompraCantidad(Integer compraCantidad) {
        this.compraCantidad = compraCantidad;
    }

    public Date getCompraFecha() {
        return compraFecha;
    }

    public void setCompraFecha(Date compraFecha) {
        this.compraFecha = compraFecha;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    @Override
    public String toString() {
        return "";
    }
}
