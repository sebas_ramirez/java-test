package com.test.java.test.entity;

import java.sql.Date;

public class Producto {

    private Integer productoId;
    private String productoDescripcion;
    private Integer productoPrecio;
    private Date productoFechaVencimiento;

    public Producto() {

    }

    public Producto(Integer productoId, String productoDescripcion, Integer productoPrecio, Date productoFechaVencimiento) {
        this.productoId = productoId;
        this.productoDescripcion = productoDescripcion;
        this.productoPrecio = productoPrecio;
        this.productoFechaVencimiento = productoFechaVencimiento;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    public String getProductoDescripcion() {
        return productoDescripcion;
    }

    public void setProductoDescripcion(String productoDescripcion) {
        this.productoDescripcion = productoDescripcion;
    }

    public Integer getProductoPrecio() {
        return productoPrecio;
    }

    public void setProductoPrecio(Integer productoPrecio) {
        this.productoPrecio = productoPrecio;
    }

    public Date getProductoFechaVencimiento() {
        return productoFechaVencimiento;
    }

    public void setProductoFechaVencimiento(Date productoFechaVencimiento) {
        this.productoFechaVencimiento = productoFechaVencimiento;
    }

    @Override
    public String toString() {
        return "productoId: " + productoId + ", productoDescripcio: " + productoDescripcion + ", productoPrecio: " + productoPrecio + ", productoFechaVencimient: " + productoFechaVencimiento;
    }

}
