package com.test.java.test.service;

import com.test.java.test.entity.Compra;
import com.test.java.test.entity.Producto;
import com.test.java.test.entity.Stock;
import com.test.java.test.repository.CompraRepository;
import com.test.java.test.repository.ProductoRepository;
import com.test.java.test.repository.StockRepository;
import com.test.java.test.util.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Objects;

public class CompraProceso {

    public Compra procesoCompra(Integer productoId, Integer productoCantidad, Date compraFecha) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();

        ProductoRepository productoRepository = new ProductoRepository();
        Producto producto_deseado = productoRepository.leerProductoPorId(productoId);

        try {

            if (Objects.isNull(producto_deseado.getProductoId())) {
                System.out.println("NO EXISTE PRODUCTO");
                return null;
            }

            StockRepository stockRepository = new StockRepository();
            Stock stock_disponible = stockRepository.leerStockId(productoId);
            Integer nuevaCantidad = stock_disponible.getStockCantidad() + productoCantidad;
            if (Objects.isNull(stock_disponible.getStockId())) {
                stockRepository.insertStock(productoId, nuevaCantidad);
            }
            stockRepository.updateStock(productoId, nuevaCantidad);

            System.out.println("COMPRADO\n");
            
            CompraRepository compraRepository = new CompraRepository();
            Compra compra = new Compra();
            compra.setCompraCantidad(productoCantidad);
            compra.setProductoId(productoId);
            compra.setCompraFecha(compraFecha);
            compraRepository.insertarCompra(compra);
            
            return new Compra(productoId, nuevaCantidad, compraFecha);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return null;
    }
}
