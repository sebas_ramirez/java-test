package com.test.java.test.service;

import com.test.java.test.entity.Producto;
import com.test.java.test.entity.ProductoEstrella;
import com.test.java.test.repository.ProductoRepository;
import com.test.java.test.util.Conexion;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductoEstrellaProceso {

    public ProductoEstrella procesoProductoEstrella() throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();

        ProductoRepository productoRepository = new ProductoRepository();

        ArrayList<Producto> ListaProductos = productoRepository.leerTodoProducto();

        try {

            Integer cantidadEstrella = 0;
            ProductoEstrella response = new ProductoEstrella();

            for (Producto item : ListaProductos) {

                ProductoEstrella productoEstrella = productoRepository.calcularProductoEstrella(item.getProductoId());
                if (productoEstrella.getProductoEstrellaCantidadVendido() > cantidadEstrella) {
                    cantidadEstrella = productoEstrella.getProductoEstrellaCantidadVendido();
                    response.setProductoEstrellaCantidadVendido(productoEstrella.getProductoEstrellaCantidadVendido());
                    response.setProductoEstrellaNombre(productoEstrella.getProductoEstrellaNombre());
                }
            }
            
            System.out.println("Producto: " + response.getProductoEstrellaNombre());
            System.out.println("cantidad vendida: " + response.getProductoEstrellaCantidadVendido().toString());
            System.out.println("fecha: " + response.getProductoEstrellaFechaReporte().toString());
            
            return response;
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }

        return null;
    }
}
