package com.test.java.test.service;

import com.test.java.test.entity.Producto;
import com.test.java.test.entity.Stock;
import com.test.java.test.entity.Venta;
import com.test.java.test.repository.ProductoRepository;
import com.test.java.test.repository.StockRepository;
import com.test.java.test.repository.VentaRepository;
import com.test.java.test.util.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Objects;

public class VentaProceso {

    public Venta procesoVenta(Integer productoId, Integer productoCantidad, Date ventaFecha) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();

        try {
            ProductoRepository productoRepository = new ProductoRepository();
            Producto producto_deseado = productoRepository.leerProductoPorId(productoId);

            if (Objects.isNull(producto_deseado.getProductoId())) {
                System.out.println("NO EXISTE PRODUCTO");
                return null;
            }

            StockRepository stockRepository = new StockRepository();
            Stock stock_disponible = stockRepository.leerStockId(productoId);

            if (stock_disponible.getStockCantidad() < productoCantidad) {
                System.out.println("NO TENEMOS STOCK");
                return null;
            }
            Integer nuevaCantidad = stock_disponible.getStockCantidad() - productoCantidad;
            stockRepository.updateStock(productoId, nuevaCantidad);

            System.out.println("VENDIDO\n");
            
            VentaRepository ventaRepository = new VentaRepository();
            Venta venta = new Venta();
            venta.setVentaCantidad(productoCantidad);
            venta.setProductoId(productoId);
            venta.setVentaFecha(ventaFecha);
            ventaRepository.insertarVenta(venta);
            
            return new Venta(productoId, nuevaCantidad, ventaFecha);
            
            

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return null;
    }
}
