package com.test.java.test.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    private static final String USER = "postgres";
    private static final String PASS = "8246";
    private static Connection connection;

    public Connection initConection() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bd_test_java", USER, PASS);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }

    public void closeConection(Connection connection) throws SQLException {

        if (connection != null) {
            connection.close();
        }
    }
}
