package com.test.java.test.repository;

import com.test.java.test.entity.Venta;
import com.test.java.test.util.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VentaRepository {

    private final String GET_VENTA_ID = "SELECT venta_id, producto_id, venta_cantidad, venta_fecha FROM \"venta\" WHERE venta_id=?";
    private final String GET_VENTA_ALL = "SELECT venta_id, producto_id, venta_cantidad, venta_fecha FROM \"venta\" ";
    private static final String INSERT_VENTA = "INSERT INTO public.venta(venta_id, producto_id, venta_cantidad, venta_fecha) VALUES (((select max(v.venta_id) +1 from public.venta v)), ?, ?, ?)";
    private static final String DELETE_VENTA = "DELETE FROM public.venta WHERE venta_id=?";
    private static final String DELETE_VENTA_ALL = "DELETE FROM public.venta";
    private static final String UPDATE_VENTA = "UPDATE public.venta SET venta_cantidad=? WHERE venta_id=?";

    public Venta leerVentaPorId(Integer id) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        ResultSet rs = null;
        conn = conexion.initConection();
        Venta venta = new Venta();
        PreparedStatement st;

        st = conn.prepareStatement(GET_VENTA_ID);
        st.setInt(1, id);

        rs = st.executeQuery();

        try {
            if (rs.next()) {
                venta.setVentaId(rs.getInt(1));
                venta.setProductoId(rs.getInt(2));
                venta.setVentaCantidad(rs.getInt(3));
                venta.setVentaFecha(rs.getDate(4));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return venta;
    }

    public ArrayList<Venta> leerTodoVenta() throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;
        Venta venta = new Venta();

        ArrayList<Venta> ventaList = new ArrayList();

        PreparedStatement st;

        st = conn.prepareStatement(GET_VENTA_ALL);
        rs = st.executeQuery();

        while (rs.next()) {

            venta.setVentaId(rs.getInt(1));
            venta.setProductoId(rs.getInt(2));
            venta.setVentaCantidad(rs.getInt(3));
            venta.setVentaFecha(rs.getDate(4));

            ventaList.add(venta);
        }
        // Primeramente crear tu conexion
        //Hacer tu consulta sql
        //Ejecutas
        //Cargar lo que te retorna a tu objeto que retornar el metodo
        return ventaList;
    }

    public void insertarVenta(Venta venta) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        PreparedStatement st;

        try {
            st = conn.prepareStatement(INSERT_VENTA);
            st.setInt(1, venta.getProductoId());
            st.setInt(2, venta.getVentaCantidad());
            st.setDate(3, venta.getVentaFecha());
            st.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
    }

    public Venta eliminarVentaId(Integer id) throws SQLException {

        Venta venta = this.leerVentaPorId(id);

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        try {

            PreparedStatement st;

            st = conn.prepareStatement(DELETE_VENTA);
            st.setInt(1, id);

            st.execute();

            return venta;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return venta;
    }

    public ArrayList<Venta> eliminarTodoVenta() throws SQLException {

        ArrayList<Venta> ventaLista = this.leerTodoVenta();

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        try {

            PreparedStatement st;

            st = conn.prepareStatement(DELETE_VENTA_ALL);
            rs = st.executeQuery();

            return ventaLista;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return ventaLista;
    }

    public void updateVenta(Integer productoId, Integer newCantidad) throws SQLException {
        
        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();

        try {

            PreparedStatement st = conn.prepareStatement(UPDATE_VENTA);

            st.setInt(1, newCantidad);
            st.setInt(2, productoId);

            st.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
    }
}