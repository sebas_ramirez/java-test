package com.test.java.test.repository;

import com.test.java.test.entity.Compra;
import com.test.java.test.util.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CompraRepository {

    private final String GET_COMPRA_ID = "SELECT compra_id, producto_id, compra_cantidad, compra_fecha FROM \"compra\" WHERE compra_id=?";
    private final String GET_COMPRA_ALL = "SELECT compra_id, producto_id, compra_cantidad, compra_fecha FROM \"compra\" ";
    private static final String INSERT_COMPRA = "INSERT INTO public.compra(compra_id, producto_id, compra_cantidad, compra_fecha) VALUES (COALESCE((select max(c.compra_id) +1 from public.compra c),1), ?, ?, ?)";
    private static final String DELETE_COMPRA = "DELETE FROM public.compra WHERE compra_id=?";
    private static final String DELETE_COMPRA_ALL = "DELETE FROM public.compra";
    private static final String UPDATE_COMPRA = "UPDATE public.compra SET producto_id=?, compra_cantidad=?, compra_fecha=? WHERE compra_id=?";

    public Compra leerCompraPorId(Integer id) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        ResultSet rs = null;
        conn = conexion.initConection();
        PreparedStatement st;
        Compra compra = new Compra();

        st = conn.prepareStatement(GET_COMPRA_ID);
        st.setInt(1, id);

        rs = st.executeQuery();

        try {
            if (rs.next()) {
                compra.setCompraId(rs.getInt(1));
                compra.setProductoId(rs.getInt(2));
                compra.setCompraCantidad(rs.getInt(3));
                compra.setCompraFecha(rs.getDate(4));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return compra;
    }

    public ArrayList<Compra> leerTodoCompra() throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;
        Compra compra = new Compra();

        ArrayList<Compra> compraList = new ArrayList();

        PreparedStatement st;

        st = conn.prepareStatement(GET_COMPRA_ALL);
        rs = st.executeQuery();

        while (rs.next()) {

            compra.setCompraId(rs.getInt(1));
            compra.setProductoId(rs.getInt(2));
            compra.setCompraCantidad(rs.getInt(3));
            compra.setCompraFecha(rs.getDate(4));

            compraList.add(compra);
        }
        return compraList;
    }

    public void insertarCompra(Compra compra) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();
        ResultSet rs = null;

        PreparedStatement st;

        try {
            st = conn.prepareStatement(INSERT_COMPRA);
            st.setInt(1, compra.getProductoId());
            st.setInt(2, compra.getCompraCantidad());
            st.setDate(3, compra.getCompraFecha());
            st.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
    }

    public Compra eliminarCompraId(Integer id) throws SQLException {

        Compra compra = this.leerCompraPorId(id);

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        try {

            PreparedStatement st;

            st = conn.prepareStatement(DELETE_COMPRA);
            st.setInt(1, id);

            st.execute();

            return compra;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return compra;
    }

    public ArrayList<Compra> eliminarTodoCompra() throws SQLException {

        ArrayList<Compra> compraLista = this.leerTodoCompra();

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        try {

            PreparedStatement st;

            st = conn.prepareStatement(DELETE_COMPRA_ALL);
            rs = st.executeQuery();

            return compraLista;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return compraLista;
    }

    public Compra modificarCompra(Compra id) throws SQLException {

        Compra compra = new Compra();

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();

        try {

            PreparedStatement st = conn.prepareStatement(UPDATE_COMPRA);

            st.setInt(1, id.getProductoId());
            st.setInt(2, id.getCompraCantidad());
            st.setDate(3, id.getCompraFecha());
            st.setInt(4, id.getCompraId());

            st.executeUpdate();

            return id;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return compra;
    }
}
