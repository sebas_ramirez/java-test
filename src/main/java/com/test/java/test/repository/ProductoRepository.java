package com.test.java.test.repository;

import com.test.java.test.entity.Producto;
import com.test.java.test.entity.ProductoEstrella;
import com.test.java.test.util.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductoRepository {

    private final String GET_PRODUCT_ID = "SELECT producto_id, producto_descripcion, producto_precio, producto_fecha_vencimiento FROM \"producto\" WHERE producto_id=?";
    private final String GET_PRODUCT_ALL = "SELECT producto_id, producto_descripcion, producto_precio, producto_fecha_vencimiento FROM \"producto\"";
    private static final String INSERT_PRODUCT = "INSERT INTO public.producto(producto_id, producto_descripcion, producto_precio, producto_fecha_vencimiento) VALUES (?, ?, ?, ?)";
    private static final String DELETE_PRODUCT = "DELETE FROM public.producto WHERE producto_id=?";
    private static final String DELETE_PRODUCT_ALL = "DELETE FROM public.producto";
    private static final String UPDATE_PRODUCT = "UPDATE public.producto SET producto_descripcion=?, producto_precio=?, producto_fecha_vencimiento=? WHERE producto_id=?";
    private static final String GET_PRODUCTO_ESTRELLA = "SELECT SUM(venta_cantidad) FROM public.venta WHERE producto_id=?";
    
    
    public Producto leerProductoPorId(Integer id) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();
        Producto producto = new Producto();

        PreparedStatement st = conn.prepareStatement(GET_PRODUCT_ID);
        st.setInt(1, id);

        ResultSet rs = st.executeQuery();

        try {
            if (rs.next()) {
                producto.setProductoId(rs.getInt(1));
                producto.setProductoDescripcion(rs.getString(2));
                producto.setProductoPrecio(rs.getInt(3));
                producto.setProductoFechaVencimiento(rs.getDate(4));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return producto;
    }

    public ArrayList<Producto> leerTodoProducto() throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();

        try {

            ArrayList<Producto> productoLista = new ArrayList();

            PreparedStatement st = conn.prepareStatement(GET_PRODUCT_ALL);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Producto producto = new Producto();

                producto.setProductoId(rs.getInt(1));
                producto.setProductoDescripcion(rs.getString(2));
                producto.setProductoPrecio(rs.getInt(3));
                producto.setProductoFechaVencimiento(rs.getDate(4));

                productoLista.add(producto);
            }
            return productoLista;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        } finally {
            conexion.closeConection(conn);
        }

        // Primeramente crear tu conexion
        //Hacer tu consulta sql
        //Ejecutas
        //Cargar lo que te retorna a tu objeto que retorna el metodo
    }

    public void insertarProducto(Producto producto) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        try {
            PreparedStatement st = conn.prepareStatement(INSERT_PRODUCT);
            st.setInt(1, producto.getProductoId());
            st.setString(2, producto.getProductoDescripcion());
            st.setInt(3, producto.getProductoPrecio());
            st.setDate(4, producto.getProductoFechaVencimiento());
            rs = st.executeQuery();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
    }

    public Producto eliminarProductoId(Integer id) throws SQLException {

        Producto producto = this.leerProductoPorId(id);

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        try {

            PreparedStatement st;

            st = conn.prepareStatement(DELETE_PRODUCT);
            st.setInt(1, id);

            st.execute();

            return producto;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return producto;
    }

    public ArrayList<Producto> eliminarTodoProducto() throws SQLException {

        ArrayList<Producto> productoLista = this.leerTodoProducto();

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        ResultSet rs = null;

        try {

            PreparedStatement st;

            st = conn.prepareStatement(DELETE_PRODUCT_ALL);
            rs = st.executeQuery();

            return productoLista;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return productoLista;
    }

    public Producto modificarProducto(Producto id) throws SQLException {

        Producto producto = new Producto();

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();

        try {

            PreparedStatement st = conn.prepareStatement(UPDATE_PRODUCT);

            st.setString(1, id.getProductoDescripcion());
            st.setInt(2, id.getProductoPrecio());
            st.setDate(3, id.getProductoFechaVencimiento());
            st.setInt(4, id.getProductoId());

            st.executeUpdate();

            return id;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return producto;
    }

    public ProductoEstrella calcularProductoEstrella(Integer productoId) throws SQLException {
        
        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();
        
        try {
            ProductoEstrella response = new ProductoEstrella();
            
            PreparedStatement st = conn.prepareStatement(GET_PRODUCTO_ESTRELLA);
            st.setInt(1, productoId);
            ResultSet rs = st.executeQuery();
            
            
            if (rs.next()) {
                
                response.setProductoEstrellaCantidadVendido(rs.getInt(1));
                response.setProductoEstrellaNombre(this.leerProductoPorId(productoId).getProductoDescripcion());
                
            }
            return response;
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        
        return null;
        
        }
}
