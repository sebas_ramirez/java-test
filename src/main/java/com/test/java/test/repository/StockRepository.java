package com.test.java.test.repository;

import com.test.java.test.entity.Stock;
import com.test.java.test.util.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StockRepository {

    private final String GET_STOCK_ID = "SELECT stock_id, producto_id, stock_cantidad FROM \"stock\" WHERE stock_id=?";
    private static final String UPDATE_STOCK = "UPDATE public.stock SET stock_cantidad=? WHERE stock_id=?";
    private static final String INSERT_STOCK = "INSERT INTO public.stock(stock_id, producto_id, stock_cantidad) VALUES (((select max(s.stock_id) +1 from public.stock s)), ?, ?)";

    public Stock leerStockId(Integer id) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();
        Stock stock = new Stock();
        stock.setStockCantidad(0);
        stock.setProductoId(id);

        PreparedStatement st = conn.prepareStatement(GET_STOCK_ID);
        st.setInt(1, id);

        ResultSet rs = st.executeQuery();

        try {

            if (rs.next()) {
                stock.setStockId(rs.getInt(1));
                stock.setProductoId(rs.getInt(2));
                stock.setStockCantidad(rs.getInt(3));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
        return stock;
    }

    public void updateStock(Integer productoId, Integer nuevaCantidad) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = conexion.initConection();

        try {

            PreparedStatement st = conn.prepareStatement(UPDATE_STOCK);

            st.setInt(1, nuevaCantidad);
            st.setInt(2, productoId);

            st.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }

    }

    public void insertStock(Integer productoId, Integer nuevaCantidad) throws SQLException {

        Conexion conexion = new Conexion();
        Connection conn = null;
        conn = conexion.initConection();

        PreparedStatement st;

        try {
            st = conn.prepareStatement(INSERT_STOCK);
            st.setInt(1, productoId);
            st.setInt(2, nuevaCantidad);
            st.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            conexion.closeConection(conn);
        }
    }
}
